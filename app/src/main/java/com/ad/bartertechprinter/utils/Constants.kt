package com.ad.bartertechprinter.utils


const val BASE_URL: String = "https://stage.bartertech.net/"
const val IMAGE_BASE_URL = "https://bartertech.net/upload/members/thumb/";

const val DEFAULT_NOTIFICATION_DISTANCE: Int = 100

//SHARED_PREFERENCES
const val REMEMBER_ME: String = "remember_me"
const val USER_EMAIL: String = "user_email"
const val USER_PASSWORD: String = "user_password"
const val NOTIFICATION_ENABLED: String = "notification_enabled"
const val BUSINESS_NOTIFICATION_DISTANCE: String = "business_notification_distance"
const val TOKEN: String = "token"