package com.ad.bartertechprinter.utils

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.appcompat.app.AlertDialog
import android.graphics.drawable.InsetDrawable
import android.view.Gravity
import android.view.WindowManager


abstract class BaseDialogHelper {

    abstract val dialogView: View
    abstract val builder: AlertDialog.Builder

    //  required bools
    open var cancelable: Boolean = true
    open var isBackGroundTransparent: Boolean = true
    open var withPadding: Boolean = true
    open var bottomGravity: Boolean = false

    //  dialog
    open var dialog: AlertDialog? = null

    //  dialog create
    open fun create(): AlertDialog {
        dialog = builder
            .setCancelable(cancelable)
            .create()

        //  very much needed for customised dialogs
        if (isBackGroundTransparent) {
            var inset = InsetDrawable(ColorDrawable(Color.TRANSPARENT), 0)
            if (withPadding) {
                inset = InsetDrawable(ColorDrawable(Color.TRANSPARENT), 20)
            }
            dialog?.window?.setBackgroundDrawable(inset)
        }

        if (bottomGravity) {
            val wlp = dialog?.window?.attributes
            wlp?.gravity = Gravity.BOTTOM
            wlp?.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
            dialog?.window?.attributes = wlp
        }

        return dialog!!
    }

}
