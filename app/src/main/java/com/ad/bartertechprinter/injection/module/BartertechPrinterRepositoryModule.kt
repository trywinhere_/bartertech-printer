package com.ad.bartertechprinter.injection.module

import android.content.SharedPreferences
import com.ad.bartertechprinter.app.BartertechPrinterApp
import com.ad.bartertechprinter.injection.data.BartertechPrinterData
import com.ad.bartertechprinter.injection.data.BartertechPrinterRepository
import com.ad.bartertechprinter.injection.data.db.TransactionsDao
import com.ad.bartertechprinter.injection.data.db.TransactionsDbModule
import com.ad.bartertechprinter.injection.scopes.AppScoped
import dagger.Module
import dagger.Provides


@Module(
    includes = [TransactionsDbModule::class]
//    includes = [LocationSourceModule::class, OrientationSourceModule::class, GPSPhotoDbModule::class, SessionSourceModule::class, AppStatusSourceModule::class,
//        NetworkModule::class, BillingClientSourceModule::class, SubscriptionDbModule::class]
)
class BartertechPrinterRepositoryModule {

    @Provides
    @AppScoped
    internal fun providesRepository(
        transactionsDao: TransactionsDao,
        sharedPreferences: SharedPreferences): BartertechPrinterRepository{
        return BartertechPrinterData(transactionsDao,
            sharedPreferences)
    }
//    @Provides
//    @AppScoped
//    internal fun providesRepository(
//        orientationDataSource: OrientationDataSource,
//        locationDataSource: LocationDataSource,
//        appStatusDataSource: AppStatusDataSource,
//        daoHelper: MultiDbDaoHelper,
//        subscriptionStatusDao: SubscriptionStatusDao,
//        sharedPreferences: SharedPreferences,
//        billingClientSource: BillingClientSource,
//        sessionRepository: SessionRepository,
//        gpsPhotoApi: GPSPhotoApi
//    ): GPSPhotoDataRepository {
//        return GPSPhotoDataDefault(
//            locationDataSource,
//            orientationDataSource,
//            appStatusDataSource,
//            daoHelper,
//            subscriptionStatusDao,
//            sharedPreferences,
//            billingClientSource,
//            sessionRepository,
//            gpsPhotoApi
//        )
//    }
}