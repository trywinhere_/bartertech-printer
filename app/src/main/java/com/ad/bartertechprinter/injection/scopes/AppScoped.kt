package com.ad.bartertechprinter.injection.scopes

import kotlin.annotation.Retention
import javax.inject.Scope
import javax.inject.Singleton

@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScoped