package com.ad.bartertechprinter.injection.module

import android.content.Context
import com.ad.bartertech.network.BartertechApi
import com.ad.bartertech.network.NetManager
import com.ad.bartertech.network.TokenInterceptor
import com.ad.bartertechprinter.utils.BASE_URL
import com.ad.bartertechprinter.injection.scopes.AppScoped
import com.ad.bartertechprinter.network.ConnectivityInterceptor
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Module which provides all required dependencies about network
 */
@Module
@Suppress("unused")
object NetworkModule {

    @AppScoped
    @Provides
    @JvmStatic
    internal fun networkManager(context: Context): NetManager {
        return NetManager(context)
    }

    @AppScoped
    @Provides
    @JvmStatic
    internal fun connectivityInterceptor(netManager: NetManager): Interceptor {
        return ConnectivityInterceptor(netManager)
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideOkhttpClient(tokenInterceptor: TokenInterceptor, connectivityInterceptor: ConnectivityInterceptor): OkHttpClient.Builder {
        return OkHttpClient.Builder().addInterceptor(tokenInterceptor).addInterceptor(connectivityInterceptor) //TODO: IMPORTANT!! Not sure if two interceptors work

    }
    /**
     * Provides the Post service implementation.
     * @param retrofit the Retrofit object used to instantiate the service
     * @return the Post service implementation.
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun providePostApi(retrofit: Retrofit): BartertechApi {
        return retrofit.create(BartertechApi::class.java)
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofitInterface(okHttpClientBuilder: OkHttpClient.Builder): Retrofit {

        okHttpClientBuilder
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClientBuilder.build())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }
}