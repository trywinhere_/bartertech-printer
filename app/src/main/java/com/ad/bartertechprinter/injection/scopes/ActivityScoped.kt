package com.ad.bartertechprinter.injection.scopes

import javax.inject.Scope

@MustBeDocumented
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScoped