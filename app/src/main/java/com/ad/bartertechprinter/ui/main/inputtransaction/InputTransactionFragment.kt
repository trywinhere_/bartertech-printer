package com.ad.bartertechprinter.ui.main.inputtransaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.ad.bartertechprinter.R
import com.ad.bartertechprinter.databinding.FragmentInputTransactionBinding
import com.ad.bartertechprinter.injection.scopes.FragmentScoped
import com.ad.bartertechprinter.ui.main.home.HomeViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

@FragmentScoped
class InputTransactionFragment : DaggerFragment() {

    private lateinit var binding: FragmentInputTransactionBinding
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var viewModel: InputTransactionViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_input_transaction, container, false)
        initiateViewModels()
        return binding.root
    }

    private fun initiateViewModels() {
        homeViewModel =
            ViewModelProvider(requireActivity(), viewModelFactory).get(HomeViewModel::class.java)
        viewModel = ViewModelProvider(requireActivity(), viewModelFactory).get(InputTransactionViewModel::class.java)

        binding.homeViewModel = homeViewModel
        binding.viewModel = viewModel
        binding.handler = this
    }

    fun onPrintTransactionClick() {
        //TODO: Print transaction action
    }

}