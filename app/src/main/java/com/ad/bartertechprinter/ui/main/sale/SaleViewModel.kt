package com.ad.bartertechprinter.ui.main.sale

import androidx.lifecycle.ViewModel
import com.ad.bartertechprinter.injection.data.BartertechPrinterRepository
import com.ad.bartertechprinter.injection.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
class SaleViewModel @Inject constructor(
    private val repository: BartertechPrinterRepository
)  : ViewModel() {

    fun makeSale() {

    }
}