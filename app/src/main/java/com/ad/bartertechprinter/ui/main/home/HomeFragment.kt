package com.ad.bartertechprinter.ui.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ad.bartertechprinter.R
import com.ad.bartertechprinter.databinding.FragmentHomeBinding
import com.ad.bartertechprinter.injection.scopes.FragmentScoped
import dagger.android.support.DaggerFragment
import javax.inject.Inject

@FragmentScoped
class HomeFragment : DaggerFragment(), HomeNavigator {

    private lateinit var binding: FragmentHomeBinding
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var viewModel: HomeViewModel
    lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        initiateViewModels()
        return binding.root
    }

    private fun initiateViewModels() {
        homeViewModel =
            ViewModelProvider(requireActivity(), viewModelFactory).get(HomeViewModel::class.java)
        viewModel = ViewModelProvider(requireActivity(), viewModelFactory).get(
            HomeViewModel::class.java)

        binding.homeViewModel = homeViewModel
        binding.viewModel = viewModel
        binding.handler = this
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.setNavigator(this)
        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
        // TODO: Use the ViewModel
    }

    fun onProcessSaleClick() {
        viewModel.navigateToSaleFragment()
    }

    fun onSettingsClick() {
        viewModel.navigateToSettingsFragment()
    }

    fun onInputTransactionClick() {
        viewModel.navigateToInputTransactionFragment()
    }

    fun onTransactionHistoryClick() {
        viewModel.navigateToHistoryFragment()
    }

    override fun navigate(navigateAction: Int) {
        navController.navigate(navigateAction)
    }

    override fun popBackStack() {
        navController.popBackStack()
    }
}
