package com.ad.bartertechprinter.ui.main.sale

import androidx.lifecycle.ViewModel
import com.ad.bartertechprinter.injection.factory.ViewModelKey
import com.ad.bartertechprinter.injection.scopes.FragmentScoped
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class SaleModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun saleFragment(): SaleFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun saleGetCardFragment(): SaleGetCardFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun confirmSaleFragment(): ConfirmSaleFragment

    @Binds
    @IntoMap
    @ViewModelKey(SaleViewModel::class)
    abstract fun bindSaleViewModel(viewModel: SaleViewModel): ViewModel
}