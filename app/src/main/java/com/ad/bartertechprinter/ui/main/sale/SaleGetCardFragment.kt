package com.ad.bartertechprinter.ui.main.sale

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.ad.bartertechprinter.R
import com.ad.bartertechprinter.databinding.FragmentSaleGetCardBinding
import com.ad.bartertechprinter.injection.scopes.FragmentScoped
import com.ad.bartertechprinter.ui.main.home.HomeViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

@FragmentScoped
class SaleGetCardFragment : DaggerFragment() {

    private lateinit var binding: FragmentSaleGetCardBinding
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var viewModel: SaleViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sale_get_card, container, false)
        initiateViewModels()
        return binding.root
    }

    private fun initiateViewModels() {
        homeViewModel =
            ViewModelProvider(requireActivity(), viewModelFactory).get(HomeViewModel::class.java)
        viewModel = ViewModelProvider(requireActivity(), viewModelFactory).get(
            SaleViewModel::class.java)

        binding.homeViewModel = homeViewModel
        binding.viewModel = viewModel
        binding.handler = this
    }

}